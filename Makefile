DOCKER_IMAGE ?= i18n-cli
DOCKER_COMMAND ?= docker run --rm -v "$(PWD)":/i18n-cli -w /i18n-cli $(DOCKER_IMAGE)
DOCKER_COMMAND_IT ?= docker run --rm -it -v "$(PWD)":/i18n-cli -w /i18n-cli -e HISTFILE=/i18n-cli/.bash-history $(DOCKER_IMAGE)

## Build docker imge
build:
	@docker build -t $(DOCKER_IMAGE) .

## Run container bash
bash:
	@$(DOCKER_COMMAND_IT) bash

## Run ruby irb console
console:
	@$(DOCKER_COMMAND_IT) bundle exec ./bin/console

## Update gems dependencies on Gemfile.lock
update-dependencies:
	@rm -f Gemfile.lock
	@$(DOCKER_COMMAND) bundle install

## Build i18n-cli gem
gem-build:
	@mkdir -p gems
	@$(DOCKER_COMMAND) bash -c "gem build i18n-cli.gemspec; mv i18n-cli*.gem gems/"

## Push i18n-cli gem. GEM=[gem-name]
gem-push:
	@docker run --rm -v "$(PWD)":/usr/src/app -w /usr/src/app -it ruby:2.7.2 gem push ./gems/$(GEM)

# @docker run --rm -it -v "$(PWD)":/usr/src/app -w /usr/src/app -it ruby:2.7.2 bash
# @$(DOCKER_COMMAND) gem push ./gems/$(GEM)

.DEFAULT_GOAL := show-help

.PHONY: show-help console

show-help:
	@echo "$$(tput bold)Commands:$$(tput sgr0)"
	@sed -n -e "/^## / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^## //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n## /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=38 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		if (length($$1) == 0) { \
		    printf "\n"; \
		} \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}'
