# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'i18n-cli/version'

Gem::Specification.new do |spec|
  spec.name          = 'i18n-cli'
  spec.version       = I18n::VERSION
  spec.authors       = ['Luciano Prestes Cavalcanti']
  spec.email         = ['lucianopcbr@gmail.com']

  spec.summary       = 'A program that manage localization keys'
  spec.description   = 'I18n CLI is a program that manage json files to' \
                       'create localization texts'
  spec.homepage      = 'https://gitlab.com/LucianoPC/i18n-cli'
  spec.license       = 'Expat'

  spec.files         = Dir['./lib/**/*']

  spec.bindir = 'bin'
  spec.executables << 'i18n'
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '2.4.10'
  spec.add_development_dependency 'rake', '13.0.6'
  spec.add_development_dependency 'rspec', '3.12.0'
  spec.add_development_dependency 'pry', '0.14.2'
end
