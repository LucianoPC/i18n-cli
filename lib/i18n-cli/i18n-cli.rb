require 'json'
require 'i18n-cli/version'

module I18n

  LOCALIZATION_FILE = 'localization.json'
  LOCALIZATION_LOCK_FILE = '.localization.lock'

  class I18n

    def run(argv)
      command = argv[0]

      case command
      when 'init'
        init(argv)
      when 'apply'
        apply
      when 'commit'
        commit
      when 'build'
        build
      else
        help
      end
    end

    def init(argv)
      project_name = argv[1]
      languages = argv[2..-1]

      localization_lock_hash = {
        data: languages.each_with_object({}) {|i, hash| hash[i] = {}},
        metadata: {
          cliVersion: VERSION,
          projectName: project_name,
          buildFolder: './',
          version: '0.1',
        }
      }

      file_content = JSON.pretty_generate(localization_lock_hash)
      File.write(LOCALIZATION_LOCK_FILE, file_content)
    end

    def apply
      file_content = File.open(LOCALIZATION_LOCK_FILE, &:read)
      localization_lock_hash = JSON.parse(file_content)
      data = localization_lock_hash['data']
      languages = data.keys
      localization_keys = data[languages[0]].keys

      keys_hash = Hash[localization_keys.map{ |k| [k, Hash[languages.map{ |l| [l, data[l][k]] }]] }]

      localization_hash = {
        strings: keys_hash,
        version: localization_lock_hash['metadata']['version'],
      }

      file_content = JSON.pretty_generate(localization_hash)
      File.write(LOCALIZATION_FILE, file_content)
    end

    def commit
      localization_hash = JSON.parse(File.open(LOCALIZATION_FILE, &:read))
      localization_lock_hash = JSON.parse(File.open(LOCALIZATION_LOCK_FILE, &:read))

      if localization_hash['version'] < localization_lock_hash['metadata']['version']
        apply
        return
      end

      keys_hash = localization_hash['strings']
      localization_keys = keys_hash.keys.sort
      languages = keys_hash[localization_keys[0]].keys

      data = Hash[languages.map{ |l| [l, Hash[localization_keys.map{ |k| [k, keys_hash[k][l]] }]] }]

      version = localization_hash['version']
      version_split = version.split('.')

      new_localization_lock_hash = {
        data: data,
        metadata: {
          cliVersion: VERSION,
          projectName: localization_lock_hash['metadata']['projectName'],
          buildFolder: localization_lock_hash['metadata']['buildFolder'],
          version: "#{version_split[0]}.#{version_split[1].to_i + 1}",
        }
      }

      file_content = JSON.pretty_generate(new_localization_lock_hash)
      File.write(LOCALIZATION_LOCK_FILE, file_content)

      apply
    end

    def build
      localization_lock_hash = JSON.parse(File.open(LOCALIZATION_LOCK_FILE, &:read))

      build_folder = localization_lock_hash['metadata']['buildFolder']

      if !File.directory?(build_folder)
        puts "Build folder '#{build_folder}' doesn't exists"
        return
      end

      FileUtils.rm_f(Dir.glob("#{build_folder}/*"))

      version = localization_lock_hash['metadata']['version']
      all_languages = localization_lock_hash['data'].keys

      localization_lock_hash['data'].each do |language_name, language_data|
        file_path = File.join(build_folder, "#{language_name}.json")
        file_content = {
          strings: language_data,
          allLanguages: all_languages,
          version: version,
        }

        File.open(file_path, 'w') {|f| f.write(JSON.pretty_generate(file_content))}
      end
    end

    def help
      puts %(
Commands:
    - init \t [project-name] [language-1] [language-2] ... [language-n]
    - apply \t Create localization.json file to edit localizations
    - commit \t Update .localization.lock with the localization.json updates
    - build \t generate each language localization file
      )
    end
  end

end
