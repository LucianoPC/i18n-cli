FROM ruby:3.2.2-slim

ENV LANG=C.UTF-8
ENV LC_ALL=C.UTF-8

RUN apt update && \
    apt install -y --no-install-recommends \
    vim \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /i18n-cli/

COPY lib/i18n-cli/version.rb ./lib/i18n-cli/
COPY Gemfile Gemfile.lock i18n-cli.gemspec ./

RUN echo "alias i18n='bundle exec /i18n-cli/bin/i18n'" > /root/.bashrc

RUN bundle install

COPY . .

CMD ["bash"]
